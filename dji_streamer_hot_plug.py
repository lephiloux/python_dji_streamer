#!/usr/bin/python3

import usb.util
import usb.core
import threading
import asyncio
import subprocess
import psutil

from aio_usb_hotplug import HotplugDetector
from aio_usb_hotplug.task import HotplugEventType
from settings import DJI_PID, DJI_VID, GOOGLE_URL
from settings import STREAMER_URL, MAX_GOOGLE
from settings import FFMPEG_PATH


USB_CONFIG = 1
USB_INTERFACE = 3
USB_ENDPOINT_CONTROL_OUT = 0x03
MAGIC_TIMEOUT_MS = 500
USB_ENDPOINT_VIDEO_IN = 0x84
USB_BUFFER_SIZE_BYTES = 1024
MAGIC = [0x52, 0x4d, 0x56, 0x54]

process = []


def read_loop(usb_dev, ffmpeg_path):

    cmd_line = [f'{ffmpeg_path}',
                '-re',
                '-i',
                '-',
                '-analyzeduration',
                '10',
                '-probesize',
                '50M',
                '-vcodec',
                'copy',
                '-flags',
                'low_delay',
                '-f',
                'flv', STREAMER_URL+f'{GOOGLE_URL[usb_dev.port_number%MAX_GOOGLE]}'
                ]

    streamer = subprocess.Popen(cmd_line, stdin=subprocess.PIPE)
    while True:
        try:
            stream = usb_dev.read(USB_ENDPOINT_VIDEO_IN, USB_BUFFER_SIZE_BYTES)
            streamer.stdin.write(bytes(stream))

        except KeyboardInterrupt:
            usb.util.dispose_resources(usb_dev)
            return
        except usb.USBError as e:
            if e.errno != 110:
                usb.util.dispose_resources(usb_dev)
                streamer.kill()
                outs, errs = streamer.communicate()
            return
        except subprocess.SubprocessError as e:
            print(e)
            usb.util.dispose_resources(usb_dev)
            return


def init_google(usb_dev):
    usb.util.claim_interface(usb_dev, USB_INTERFACE)
    c_config = usb_dev.get_active_configuration()
    c_intf = c_config.interfaces()[USB_INTERFACE]
    c_ep = c_intf.endpoints()[0]
    s = c_ep.write(bytes(MAGIC), MAGIC_TIMEOUT_MS)


def use_device(device_info):

    print(device_info)
    google = device_info['device']

    current_process = psutil.Process()
    current_process.cpu_affinity([google.port_number % psutil.cpu_count()])

    print(f"google usb hub port = {google.port_number}")
    try:
        if google.is_kernel_driver_active(0):
            google.detach_kernel_driver(0)

        if google.is_kernel_driver_active(2):
            google.detach_kernel_driver(2)

        if google.is_kernel_driver_active(4):
            google.detach_kernel_driver(4)

        google.set_configuration(USB_CONFIG)

        init_google(google)
        read_loop(google, FFMPEG_PATH)

    except KeyboardInterrupt:
        pass
    except usb.USBError as e:
        if e.errno != 110:
            print(e)
    finally:
        usb.util.dispose_resources(google)
        google.attach_kernel_driver(0)
        google.attach_kernel_driver(2)
        google.attach_kernel_driver(4)
    return


async def handle_events():
    detector = HotplugDetector.for_device(vid=DJI_VID, pid=DJI_PID)
    async for event in detector.events():

        if (event.type == HotplugEventType.ADDED):
            print(repr(event))
            p = {'process': None,
                 'device': event.device,
                 'stopped': False}
            x = threading.Thread(target=use_device, args=(p,))
            x.start()
            p['process'] = x
            process.append(p)

        if (event.type == HotplugEventType.REMOVED):
            print(repr(event))
            for p in process:
                if p['device'] == event.device:
                    p['stopped'] = True
                    p['process'].join()
                    print("processes halted")

asyncio.run(handle_events())
