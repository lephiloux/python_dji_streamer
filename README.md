# python_dji_streamer

This is Linux python deamon that get DJI google stream from  google V1 and V2 (not google 2 and integra) connected to usb port.
This code is based on the code of https://github.com/fpvout/fpvout-c
But the deamon handle multiple connected google to the connected host. 

To be able to transmit the stream in various format the stream is encoded with ffmpeg to an rtsp/rtmp server
the best one for this is https://github.com/bluenviron/mediamtx. 

You can then get the stream with vlc or obs.

## Getting started
### install python required modules
pip install -r requirements.txt
### start the mediamtx server
./mediamtx 
### start the deamon
pyhton3 ./dji_streamer_hotplug.py

## Usage 
Normaly each time a google is connected to the usb bus the deamon will handle it and start the ffmpeg streaming to the local mediamtx server. 

## config file
The file settings.py contain a set of variable that will modify the beahviour of the deamon. This is a self documented file.



